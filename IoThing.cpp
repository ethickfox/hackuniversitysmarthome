#include "IoThing.h"


IoThing::IoThing()
{
	name = QString("default");
	stats.insert("Empty property", "");
}


IoThing::IoThing(QString name)
{
	this->name = name;
}

void IoThing::addStat(QString key, QString value)
{
	if (!stats.contains(key))
		stats.insert(key, value);
}

void IoThing::removeStat(QString key)
{
	stats.remove(key);
}

QString IoThing::getStat(QString key)
{
	if (stats.contains(key))
		return stats.value(key);
	else
		return QString();
}

bool IoThing::changeState(QString key, QString value)
{
	if (!stats.contains(key))
		return false;
	stats.insert(key, value);
	return true;
}


IoThing::~IoThing()
{
}

IoThing init(IoThing & thing, QString name, QMap<QString, QString> stats)
{
	thing.name = name;
	thing.stats = stats;
	return thing;
}
