#ifndef MAINSCENE_H
#define MAINSCENE_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include "sceneobject.h"

class MainScene : public QGraphicsView
{
public:

	MainScene(QWidget * parent);
	~MainScene();
	void addObject(SceneObject* newObj) { scnObjList.append(newObj); }
	QGraphicsScene *innerScene;
    QVector <SceneObject*> getObjects(){return scnObjList;}
private:
	SceneObject* lastSelected;

	void connectObjects(SceneObject* obj1, SceneObject* obj2);
	void disconnectObjects(SceneObject* obj1, SceneObject* obj2);
	bool areConnected(SceneObject* obj1, SceneObject* obj2);
private:
	QVector <SceneObject*> scnObjList;
public slots:
	void mousePressEvent(QMouseEvent* event);
    SceneObject* createObject(int type);
	void clickedOnObject(SceneObject* sender);
};

#endif // MAINSCENE_H
