#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <QWidget>
#include <QPainter>
#include <QIcon>
#include <QDebug>
#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QMouseEvent>
#include <QMenu>
//#include "mainscene.h"

class SceneObject : public QWidget
{
	Q_OBJECT

private:
	QSet <SceneObject*> connectedObjects;
	QPolygonF  drawPoly;
	QAction* actionDelete;
	QAction* actionCopy;
	QMenu* menu;
    QMap <QString, QString> objectProperties;
    QPixmap pixmap;
    int objType;


	bool _isSelected;
	bool _isConnectSelected;
    bool _isOn;

	// internal
	void selectConnected();
	void deselectConnected();

	void setSelected(bool state);
	void setConnectSelected(bool state);
    void init(int type);
public:
    explicit SceneObject(QWidget *parent, QPoint pos, int type);
	~SceneObject();
	bool isMove;
	QPoint delta;

	void select();
	void deselect();

	void addConnection(SceneObject* so);
	void removeConnection(SceneObject* so);
	bool isConnected(SceneObject* so);

	bool isSelected();
	bool isConnectSelected();
    QMap <QString, QString> getProperties(){return objectProperties;}
    QSet <SceneObject*> getConnectedObjects(){return connectedObjects;}

signals:
	void onObjectPressEvent(SceneObject*);
public slots:
	void paintEvent(QPaintEvent * event);
	void mousePressEvent(QMouseEvent * event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void slotDelete();
	void slotCopy();
    void onOffObj();

};

#endif // SCENEOBJECT_H
