#include "sceneobject.h"
#include "mainscene.h"
#include <QDesktopServices>
#define SIZE 40




SceneObject::SceneObject(QWidget *parent, QPoint pos,int type =0) :QWidget(parent)
{
//	drawPoly << QPointF(0, 39) << QPointF(39, 39) << QPointF(39, 30)
//		<< QPointF(26, 30) << QPointF(26, 0) << QPointF(14, 0) << QPointF(14, 30)
//		<< QPointF(0, 30);


    objType = type;

    menu = new QMenu(this);
    actionCopy = new QAction(QString("Копировать"), this);
    connect(actionCopy, SIGNAL(triggered()), this, SLOT(slotCopy()));
    actionCopy->setShortcut(QKeySequence("Ctrl+C"));
    menu->addAction(actionCopy);
    actionDelete = new QAction(QString("Удалить"), this);
    connect(actionDelete, SIGNAL(triggered()), this, SLOT(slotDelete()));
    actionCopy->setShortcut(QKeySequence("Ctrl+D"));
    menu->addAction(actionDelete);

    this->resize(SIZE, SIZE);
    this->move(pos);
    this->show();
    isMove = false;

    _isSelected = false;
    _isConnectSelected = false;
    _isOn = false;

    connect(this, &SceneObject::onObjectPressEvent, (MainScene*)(this->parentWidget()), &MainScene::clickedOnObject);

    init(objType);
}

void SceneObject::selectConnected()
{
    QSet<SceneObject*>::iterator it = connectedObjects.begin();
    while (it != connectedObjects.end())
    {
        (*it)->setConnectSelected(true);
        it++;
    }
    if (_isConnectSelected)
        switch(objType){
        case 0:
            pixmap = QPixmap("res/lampconsel.png");
            break;
        case 1:
            pixmap = QPixmap("res/tempconsel.png");
            break;
        default:
            pixmap = QPixmap("res/ventconsel.png");
            break;
        }
    if (_isSelected)
        switch(objType){
        case 0:
            pixmap = QPixmap("res/lampsel.png");
            break;
        case 1:
            pixmap = QPixmap("res/tempsel.png");
            break;
        default:
            pixmap = QPixmap("res/ventsel.png");
            break;
        }
    this->repaint();

}

void SceneObject::deselectConnected()
{
    QSet<SceneObject*>::iterator it = connectedObjects.begin();
    while (it != connectedObjects.end())
    {
        (*it)->setConnectSelected(false);
        it++;
    }
    if(!_isOn){

        switch (objType)
        {
        case 0:
            pixmap = QPixmap("res/lampoff.png");
            break;
        case 1:
            pixmap = QPixmap("res/tempoff.png");
            break;
        case 2:
            pixmap = QPixmap("res/ventoff.png");
            break;
        }
    }else{
        switch(objType){
        case 0:
            pixmap = QPixmap("res/lampon.png");
            break;
        case 1:
            pixmap = QPixmap("res/tempon.png");
            break;
        default:
            pixmap = QPixmap("res/venton.png");
            break;
        }
    }
    this->repaint();
}
SceneObject::~SceneObject()
{

    //        delete actionDelete;
    //        delete actionCopy;
    //        delete menu;

}
void SceneObject::select()
{
    setSelected(true);
    this->repaint();
}
void SceneObject::deselect()
{
    setSelected(false);

}
void SceneObject::addConnection(SceneObject * so)
{
    if (!connectedObjects.contains(so))
        connectedObjects.insert(so);
}
void SceneObject::removeConnection(SceneObject * so)
{
    if (connectedObjects.contains(so))
        connectedObjects.remove(so);
}
bool SceneObject::isConnected(SceneObject * so)
{
    return connectedObjects.contains(so);
}
void SceneObject::setSelected(bool state)
{
    _isSelected = state;
    if (state)
        selectConnected();

    else

        deselectConnected();
    this->repaint();
}
void SceneObject::setConnectSelected(bool state)
{
    _isConnectSelected = state;
    this->repaint();
}

void SceneObject::init(int type)
{
    switch(type){
    case 0:
        objectProperties.insert("Имя","Умная лампа");
        objectProperties.insert("Состояне","Выключена");
        objectProperties.insert("Яркость","2");
        pixmap = QPixmap("res/lampoff.png");
        break;
    case 1:
        objectProperties.insert("Имя","Умный термометр");
        objectProperties.insert("Состояне","Выключена");
        objectProperties.insert("Температура","26");
        pixmap = QPixmap("res/tempoff.png");
        break;
    default:
        objectProperties.insert("Имя","Умный вентиилятор");
        objectProperties.insert("Состояне","Выключена");
        objectProperties.insert("Скорость(об/с)","10");
        pixmap = QPixmap("res/ventoff.png");
        break;

    }
}
bool SceneObject::isSelected()
{

    return _isSelected;
}
bool SceneObject::isConnectSelected()
{
    return _isConnectSelected;
}

void SceneObject::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
//    if
    if(_isOn)
        switch(objType){
        case 0:
            pixmap = QPixmap("res/lampon.png");
            break;
        case 1:
            pixmap = QPixmap("res/tempon.png");
            break;
        default:
            pixmap = QPixmap("res/venton.png");
            break;
        }else {
        switch(objType){
        case 0:
            pixmap = QPixmap("res/lampoff.png");
            break;
        case 1:
            pixmap = QPixmap("res/tempoff.png");
            break;
        default:
            pixmap = QPixmap("res/ventoff.png");
            break;
    }
    }
    if (_isConnectSelected)
        switch(objType){
        case 0:
            pixmap = QPixmap("res/lampconsel.png");
            break;
        case 1:
            pixmap = QPixmap("res/tempconsel.png");
            break;
        default:
            pixmap = QPixmap("res/ventconsel.png");
            break;
        }
    if (_isSelected)
        switch(objType){
        case 0:
            pixmap = QPixmap("res/lampsel.png");
            break;
        case 1:
            pixmap = QPixmap("res/tempsel.png");
            break;
        default:
            pixmap = QPixmap("res/ventsel.png");
            break;
        }
    painter.drawPixmap(0,0,40,40, pixmap);

    painter.end();
}

void SceneObject::mousePressEvent(QMouseEvent *event)
{
    switch (event->button())
    {
    case Qt::RightButton:
        menu->popup(event->globalPos());
        break;
    case Qt::LeftButton:
        isMove = true;

        delta = event->globalPos() - this->pos();
        emit onObjectPressEvent(this);
        break;
    case Qt::MiddleButton:
        slotDelete();
        break;
    default:
        ;
    }

}

void SceneObject::mouseMoveEvent(QMouseEvent *event)
{
    //    if(event->button()==Qt::le)

    QPoint point = event->globalPos() - delta;
    int parentWidth = this->parentWidget()->width();
    int parentHeight = this->parentWidget()->height();
    int width = this->width();
    int height = this->height();
    if (isMove)
    {
        if (point.x() < 0)
        {
            point.setX(0);
        }
        if (point.x() > (parentWidth - width))
        {
            point.setX(parentWidth - width);
        }

        if (point.y() < 0)
        {
            point.setY(0);
        }
        if (point.y() > (parentHeight - height))
        {
            point.setY(parentHeight - height);
        }
        this->move(point);
    }
}
void SceneObject::mouseReleaseEvent(QMouseEvent *event)
{
    if (isMove)
        isMove = false;
}
void SceneObject::slotDelete()
{
    this->deleteLater();
}


void SceneObject::slotCopy()
{
    //	SceneWidget* parent = (SceneWidget*)this->parentWidget();
    //	parent->setBuf(this);
}

void SceneObject::onOffObj()
{
    qDebug()<<"onoff";


    if(_isOn){

        switch (objType)
        {
        case 0:
            pixmap = QPixmap("res/lampoff.png");
            QDesktopServices::openUrl(QUrl("https://sandbox.rightech.io/#?m=objects&n=turnon&v=map&id=5c9655ea9684b30011ec4213&p=commands"));
            system("python leshat.py base/lamp/state");

            break;
        case 1:
            pixmap = QPixmap("res/tempoff.png");
            QDesktopServices::openUrl(QUrl("https://sandbox.rightech.io/#?m=objects&n=turnon&v=map&id=5c9655ea9684b30011ec4213&p=commands"));
            system("python leshat.py base/conder/power");
            break;
        case 2:
            pixmap = QPixmap("res/ventoff.png");
            QDesktopServices::openUrl(QUrl("https://sandbox.rightech.io/#?m=objects&n=turnon&v=map&id=5c9655ea9684b30011ec4213&p=commands"));
            system("python leshat.py base/term/state");
            break;
        }
        _isOn = false;
    }
    else{
        switch(objType){
        case 0:
            pixmap = QPixmap("res/lampon.png");
             QDesktopServices::openUrl(QUrl("https://sandbox.rightech.io/#?m=objects&n=turnon&v=map&id=5c9655ea9684b30011ec4213&p=commands"));
            system("python leshat.py base/lamp/state");

            break;
        case 1:
            pixmap = QPixmap("res/tempon.png");
            QDesktopServices::openUrl(QUrl("https://sandbox.rightech.io/#?m=objects&n=turnon&v=map&id=5c9655ea9684b30011ec4213&p=commands"));
            system("python leshat.py base/conder/power");
            break;
        default:
            pixmap = QPixmap("res/venton.png");
            QDesktopServices::openUrl(QUrl("https://sandbox.rightech.io/#?m=objects&n=turnon&v=map&id=5c9655ea9684b30011ec4213&p=commands"));
            system("python leshat.py base/term/state");
            break;
        }
        _isOn = true;

    }
    qDebug()<<"is onn"<<_isOn;
    this->repaint();

}
