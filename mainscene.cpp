#include "mainscene.h"

#include <exception>

MainScene::MainScene(QWidget *parent) : QGraphicsView(parent)
{

	lastSelected = nullptr;

	innerScene = new QGraphicsScene;
	this->setScene(innerScene);
	/*SceneObject *obj1 = createObject(), *obj2 = createObject();
	obj2->move(QPoint(30, 30));
	connectObjects(obj1, obj2);
	obj1->select();
	lastSelected = obj1;*/
}

MainScene::~MainScene()
{
	QVector<SceneObject*>::iterator it = scnObjList.begin();
	while (it != scnObjList.end())
	{
		delete *it;
		it++;
	}
}

void MainScene::connectObjects(SceneObject * obj1, SceneObject * obj2)
{
	obj1->addConnection(obj2);
	obj2->addConnection(obj1);
}

void MainScene::disconnectObjects(SceneObject * obj1, SceneObject * obj2)
{
	obj1->removeConnection(obj2);
	obj2->removeConnection(obj1);
}

bool MainScene::areConnected(SceneObject * obj1, SceneObject * obj2)
{
	//if (obj1->isConnected(obj2) != obj2->isConnected(obj1))
//		throw std::exception ("Object connection exception");
	return obj1->isConnected(obj2);
}

void MainScene::clickedOnObject(SceneObject * sender)
{
	if (sender)
	{
		if (lastSelected)
			lastSelected->deselect();
		lastSelected = sender;
		lastSelected->select();
	}
}

SceneObject *MainScene::createObject(int type = 0)
{
	QPoint createPlace(0, 0);
    SceneObject* newObject = new SceneObject(this, createPlace,type);
	scnObjList.append(newObject);
	return newObject;
}


void MainScene::mousePressEvent(QMouseEvent* event)
{
	/*switch (event->button)
	{

	}*/
	if (lastSelected)
		lastSelected->deselect();
	lastSelected = nullptr;
}
