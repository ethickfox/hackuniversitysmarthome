#pragma once
#include <QString>
#include <QMap>

class IoThing
{
protected:
	QString name;
	QMap<QString, QString> stats;
public:
	IoThing();
	IoThing(QString name);
	~IoThing();
	void addStat(QString key, QString value);
	void removeStat(QString key);
	QString getStat(QString key);
	bool changeState(QString key, QString value);
	friend IoThing init(IoThing &thing, QString name, QMap<QString, QString> stats);
};

