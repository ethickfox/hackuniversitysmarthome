#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QNetworkAccessManager>
#include <QTableWidget>
#include <QCheckBox>

#include "mainscene.h"
#include "objectslist.h"


namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:

	MainScene *scene;
//	ObjectsList *objectSpecs;
    QTableWidget *table;

	Ui::MainWindow *ui;
	QStackedWidget  *stackedWidget;
	void initconnections();
	void initwidgets();
	qreal scale;
	QNetworkAccessManager *networkManager;

private slots:
	//void onResult(QNetworkReply *reply);
    void createObject();
    void showTable(SceneObject *emiter);
    void showWindow1();
    void showWindow2();
    void showWindow3();
    void addObject(int type);
    void addObject1();
    void addObject2();
    void addObject3();

	//protected:
	//     void wheelEvent ( QWheelEvent * event );
	//     void paintEvent ( QPaintEvent * event );
};

#endif // MAINWINDOW_H
