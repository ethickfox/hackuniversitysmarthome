#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsSvgItem>
#include <graphics_view_zoom.h>


#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QUrl>
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	initwidgets();
	initconnections();
    //ui->stackedWidget->setCurrentIndex(0);
	networkManager = new QNetworkAccessManager();
	/*connect(networkManager, &QNetworkAccessManager::finished, this, &MainWindow::onResult);*/
	networkManager->get(QNetworkRequest(QUrl("http://www.evileg.ru/it_example.json")));
	/*ui->textEdit->setVisible(false);*/

}

//void MainWindow::onResult(QNetworkReply *reply)
//{
//	if (!reply->error()) {
//
//		QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
//
//		QJsonObject root = document.object();
//
//		ui->textEdit->append(root.keys().at(0) + ": " + root.value(root.keys().at(0)).toString());
//
//		QJsonValue jv = root.value("employees");
//		if (jv.isArray()) {
//			QJsonArray ja = jv.toArray();
//			for (int i = 0; i < ja.count(); i++) {
//				QJsonObject subtree = ja.at(i).toObject();
//				ui->textEdit->append(subtree.value("firstName").toString() +
//					" " +
//					subtree.value("lastName").toString());
//			}
//		}
//		ui->textEdit->append(QString::number(root.value("number").toInt()));
//	}
//    reply->deleteLater();
//}

void MainWindow::createObject()
{
    SceneObject *tmp = scene->createObject(0);
    connect(tmp,&SceneObject::onObjectPressEvent,this,&MainWindow::showTable);
}

void MainWindow::showTable(SceneObject *emiter)
{
    table->setRowCount(0);
    table->show();
    QTableWidgetItem* newItem;
    foreach (QString key, emiter->getProperties().keys()) {
        qDebug()<<key;
        table->insertRow(table->rowCount());
        newItem =new QTableWidgetItem();
        newItem->setText(key);
        table->setItem(table->rowCount()-1,0,newItem);

        newItem =new QTableWidgetItem();
        newItem->setText(emiter->getProperties().value(key));
        table->setItem(table->rowCount()-1,1,newItem);
    }




    table->insertRow(table->rowCount());

    QPushButton* btnOn = new QPushButton();
    btnOn->setText("Включить/выкл");
//    connect(btn_edit, &QPushButton::clicked, this, &MainWindow::showWindow3);

//    доделать
    connect(btnOn, &QPushButton::clicked, emiter, &SceneObject::onOffObj);

    newItem = new QTableWidgetItem();
    newItem = table->item(table->rowCount()-1,0);

    table->setItem(table->rowCount()-1,0,newItem);

    newItem = new QTableWidgetItem();
    newItem = table->item(table->rowCount()-1,0);
    table->setItem(table->rowCount()-1,1,newItem);

    table->setSpan(table->rowCount()-1,0,1,2);

    QWidget* pWidget = new QWidget();
    QHBoxLayout* pLayout = new QHBoxLayout(pWidget);
    pLayout->addWidget(btnOn);
    pLayout->setAlignment(Qt::AlignCenter);
    pLayout->setContentsMargins(0, 0, 0, 0);
    pWidget->setLayout(pLayout);

    table->setCellWidget(table->rowCount()-1,0,btnOn);




    table->insertRow(table->rowCount());

    QPushButton* btn_edit = new QPushButton();
    btn_edit->setText("Редактировать");
//    доделать
    connect(btn_edit, &QPushButton::clicked, this, &MainWindow::showWindow3);

    newItem = new QTableWidgetItem();
    newItem = table->item(table->rowCount()-1,0);

    table->setItem(table->rowCount()-1,0,newItem);

    newItem = new QTableWidgetItem();
    newItem = table->item(table->rowCount()-1,0);
    table->setItem(table->rowCount()-1,1,newItem);

    table->setSpan(table->rowCount()-1,0,1,2);

    QWidget* pWidget2 = new QWidget();
    QHBoxLayout* pLayout2 = new QHBoxLayout(pWidget);
    pLayout2->addWidget(btn_edit);
    pLayout2->setAlignment(Qt::AlignCenter);
    pLayout2->setContentsMargins(0, 0, 0, 0);
    pWidget2->setLayout(pLayout);

    table->setCellWidget(table->rowCount()-1,0,btn_edit);



    table->insertRow(table->rowCount());
    newItem =new QTableWidgetItem();
    newItem->setText("Связи");
    table->setItem(table->rowCount()-1,0,newItem);

    newItem =new QTableWidgetItem();
    table->setItem(table->rowCount()-1,1,newItem);

    table->setSpan(table->rowCount()-1,0,1,2);

    foreach (SceneObject* iter, scene->getObjects()) {
        if(emiter!=iter){
            table->insertRow(table->rowCount());
            newItem =new QTableWidgetItem();
            newItem->setText(iter->getProperties().value("Имя"));
            table->setItem(table->rowCount()-1,0,newItem);

            newItem =new QTableWidgetItem();

            QCheckBox* chck = new QCheckBox();
            connect(chck,&QCheckBox::stateChanged,emiter,[emiter,chck](int num){
                emiter->addConnection(((MainScene*)emiter->parentWidget())->getObjects().at(num-1));
                ((MainScene*)emiter->parentWidget())->getObjects().at(num-1)->addConnection(emiter);
                ((MainScene*)emiter->parentWidget())->getObjects().at(num-1)->isSelected();
                ((MainScene*)emiter->parentWidget())->getObjects().at(num-1)->repaint();
                emiter->repaint();
            });

            QWidget* wdgt = new QWidget();
            QHBoxLayout* lyt = new QHBoxLayout(pWidget);
            lyt->addWidget(chck);
            lyt->setAlignment(Qt::AlignCenter);
            wdgt->setLayout(lyt);

            table->setCellWidget(table->rowCount()-1,1,chck);
        }
    }

}

void MainWindow::showWindow2()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::showWindow1()
{
    ui->stackedWidget->setCurrentIndex(0);
}
void MainWindow::showWindow3()
{
    ui->stackedWidget->setCurrentIndex(3);
}

void MainWindow::addObject(int type)
{
    ui->stackedWidget->setCurrentIndex(0);
    SceneObject *tmp = scene->createObject(type);
    connect(tmp,&SceneObject::onObjectPressEvent,this,&MainWindow::showTable);
}

void MainWindow::addObject1()
{
    addObject(0);
}
void MainWindow::addObject2()
{
    addObject(1);
}
void MainWindow::addObject3()
{
    addObject(2);
}
MainWindow::~MainWindow()
{
	delete ui;
}


void MainWindow::initwidgets()
{
	scene = new MainScene(this);
//	objectSpecs = new ObjectsList(this);
	ui->graphicsLayout->addWidget(scene);
//	ui->specsLayout->addWidget(objectSpecs);
//    objectSpecs->hide();

	//   QGraphicsScene *scene = new QGraphicsScene();

    QGraphicsSvgItem *item = new QGraphicsSvgItem("res/example.svg");
	scene->innerScene->addItem(item);
	//    ui->graphicsLayout->setScene(scene->innerScene);

		//item->setTransform(QTransform::fromScale(3,3), true);
	Graphics_view_zoom* z = new Graphics_view_zoom(scene);
	z->set_modifiers(Qt::NoModifier);

    table = new QTableWidget();
    ui->specsLayout->addWidget(table);
    table->insertColumn(0);
    table->insertColumn(1);
    QStringList headerList = {"Название","Значение"};
    table->setHorizontalHeaderLabels(headerList);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->hide();

}


void MainWindow::initconnections()
{
    connect(ui->addItem, &QAction::triggered, this, &MainWindow::showWindow2);
    //    доделать
    connect(ui->actionHome_2, &QAction::triggered, this, &MainWindow::showWindow1);
    connect(ui->pushButton_10, &QPushButton::clicked, this, &MainWindow::addObject1 );
    connect(ui->pushButton_2, &QPushButton::clicked, this, &MainWindow::addObject2 );
    connect(ui->pushButton_12, &QPushButton::clicked, this, &MainWindow::addObject3);



//    connect(ui->
//    connect(ui->addItem, &QAction::triggered,ui->page3, &QWidget::)
}
