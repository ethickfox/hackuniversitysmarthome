import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import requests
import json
import sys
import ssl

auth = {
    'username': "test",
    'password': "Janspb1998"
}

dict_of_items = {}

for arg in sys.argv[1:]:
    dict_of_topicks = {}
    msg = subscribe.simple(arg, qos=0, msg_count=1, retained=False, hostname="sandbox.rightech.io",
                           port=1883, client_id="room1", keepalive=60, will=None, auth=auth, tls=None,
                           protocol=mqtt.MQTTv311)
    dict_of_topicks[msg.topic.split('/')[2]] = msg.payload.decode("utf-8")
    publish.single(arg,
                   payload=msg.payload,
                   hostname="sandbox.rightech.io",
                   client_id="room1",
                   auth=auth,
                   port=1883,
                   protocol=mqtt.MQTTv311)
    dict_of_items[msg.topic.split('/')[1]] = dict_of_topicks
file = open('output.json', 'w').write(json.dumps(dict_of_items))